// SPDX-License-Identifier: MIT
pragma solidity 0.8.16;

contract QuizFactory {
    address[] public quizzes;

    function createQuiz() public {}
}

contract Quiz {
    address public owner;
    string public question;
    bytes32 public sealedAnswer;

    constructor() {}

    modifier onlyOwner() {
        _;
    }

    modifier onlyPeriod(uint256 fromDays, uint256 toDays) {
        _;
    }

    function compareStrings(string memory _a, string memory _b) 
        private
        pure
        returns (bool) { }

    function commitGuess(bytes32 _sealedGuess) public payable onlyPeriod(0, 6) {

    function revealAnswer(
        string calldata _revealedAnswer
    ) public onlyOwner onlyPeriod(7, 14) { }

    function revealGuess(
        string calldata _revealedGuess
    ) public onlyPeriod(14, 21) { }


    function withdrawPrize() public onlyPeriod(21, 27) {

    function destroyQuiz() public onlyOwner onlyPeriod(28, 365) { }
}
